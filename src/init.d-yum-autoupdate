#!/bin/bash
#
# apt-autoupdate    This shell script enables the automatic
#                   system (YUM) updates
#
# Author:       Jaroslaw.Polok@cern.ch
#
# chkconfig:    345 11 89
#
# description:  Enable daily run of yum, a program updater.
# processname:  yum-autoupdate
# config: /etc/sysconfig/yum-autoupdate
#

# source function library
. /etc/rc.d/init.d/functions

lockfile=/var/lock/subsys/yum-autoupdate

RETVAL=0

start() {
        echo -n $"Enabling automatic system update (YUM): "
        touch "$lockfile" && success || failure
        RETVAL=$?
        echo
        [ -x /usr/bin/plymouth ] && /usr/bin/plymouth --hide-splash
        YUMINTERACTIVE=0 YUMBOOTRUN=1 /usr/sbin/yum-autoupdate
        [ -x /usr/bin/plymouth ] && /usr/bin/plymouth --show-splash
}

stop() {
        echo -n $"Disabling automatic system update (YUM): "
        rm -f "$lockfile" && success || failure
        RETVAL=$?
        echo
}

restart() {
        stop
        start
}

case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart|force-reload)
        restart
        ;;
  reload)
        ;;
  condrestart)
        [ -f "$lockfile" ] && restart
        ;;
  status)
        if [ -f $lockfile ]; then
                echo $"Automatic system update (YUM) is enabled."
                RETVAL=0
        else
                echo $"Automatic system update (YUM) is disabled."
                RETVAL=3
        fi
        ;;
  *)
        echo $"Usage: $0 {start|stop|status|restart|reload|force-reload|
condrestart}"
        exit 1
esac

exit $RETVAL


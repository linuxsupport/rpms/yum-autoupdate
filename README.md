# yum-autoupdate

yum-autoupdate is a set of tools to ensure automated updates on CC7 machines

On C8+ machines, the rpm:
* Provides the package name `dnf-autoupdate`
* Installs dnf-automatic as a requirement to the rpm
* Enables the systemd `dnf-automatic-install.timer` timer

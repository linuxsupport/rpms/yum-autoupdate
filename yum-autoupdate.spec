Name: yum-autoupdate
Epoch: 6
Version: 4.6.1
Release: 3%{?dist}
Summary: yum/dnf autoupdate configuration for CERN distributions
Group: Applications/System
Source: %{name}-%{version}.tgz
License: GPL
Vendor: CERN
Packager: Linux.Support@cern.ch
Url: http://cern.ch/linux/

# without this, we cannot use systemd macros
BuildRequires: systemd

%if 0%{?rhel} >= 7
Requires: cronie
Requires: systemd
Requires: epel-release
Requires: redhat-release
Obsoletes: yum-firstboot
%else
Requires: vixie-cron
Requires: chkconfig, initscripts
%endif

%if 0%{?rhel} <= 7
Requires: sed, nc, expect mailx
Requires: python, yum-changelog, yum-kernel-module, yum-protectbase, yum-tsflags, yum-utils, yum-versionlock
%endif

%if 0%{?rhel} >= 8
Requires: dnf-utils, dnf-plugins-core, dnf-automatic, python3
Provides: dnf-autoupdate
%endif

BuildRoot: %{_tmppath}/%{name}-%{PACKAGE_VERSION}
BuildArch: noarch

%define destconf etc/

%description
The %{name} package contains configuration to perform
automatic system updates using yum/dnf.

%prep

%if 0%{?rhel} == 7
%setup -q
%endif

%build

rm -rf $RPM_BUILD_ROOT

%install

%if 0%{?rhel} == 7
mkdir -p $RPM_BUILD_ROOT/usr/sbin/
mkdir -p $RPM_BUILD_ROOT/etc/
mkdir -p $RPM_BUILD_ROOT/etc/sysconfig/
mkdir -p $RPM_BUILD_ROOT/etc/cron.hourly/
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d/

cp yum-autoupdate             $RPM_BUILD_ROOT/usr/sbin/yum-autoupdate
cp sysconfig-yum-autoupdate   $RPM_BUILD_ROOT/etc/sysconfig/yum-autoupdate
cp cron.hourly-yum-autoupdate $RPM_BUILD_ROOT/etc/cron.hourly/yum-autoupdate
cp service-yum-autoupdate     $RPM_BUILD_ROOT/usr/lib/systemd/system/yum-autoupdate.service
cp logrotate.d-yum-autoupdate $RPM_BUILD_ROOT/etc/logrotate.d/yum-autoupdate
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post

%if 0%{?rhel} == 7
/usr/bin/sed -r -i 's/^YUMRANDOMWAIT=[0-9]+.*$/YUMRANDOMWAIT=59/' /etc/sysconfig/yum-autoupdate || :
/usr/bin/sed  -i 's/^YUMONBOOT=1/YUMONBOOT=0/' /etc/sysconfig/yum-autoupdate || :

if [ $1 -eq 1 ] ; then
    # Initial installation
    /usr/bin/systemctl enable yum-autoupdate.service  >/dev/null 2>&1 || :
fi
%endif

%if 0%{?rhel} >= 8
    /usr/bin/systemctl enable --now dnf-automatic-install.timer >/dev/null 2>&1 || :
    /usr/bin/systemctl --no-reload disable yum-autoupdate.service >/dev/null 2>&1 || :
    /usr/bin/systemctl stop yum-autoupdate.service >/dev/null 2>&1 || :
%endif

%preun
%if 0%{?rhel} == 7
if [ $1 -eq 0 ] ; then
        # Package removal, not upgrade
        /usr/bin/systemctl --no-reload disable yum-autoupdate.service  > /dev/null 2>&1 || :
        /usr/bin/systemctl stop yum-autoupdate.service  > /dev/null 2>&1 || :
fi
%endif

%if 0%{?rhel} >= 8
%systemd_preun dnf-automatic-install.timer
%systemd_preun yum-autoupdate.service
%endif

%postun
/usr/bin/systemctl daemon-reload >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%if 0%{?rhel} == 7
%doc README.yum-autoupdate yum-autoupdate.example-config
/usr/sbin/yum-autoupdate
%config(noreplace) /etc/sysconfig/yum-autoupdate
%config(noreplace) /etc/cron.hourly/yum-autoupdate
/usr/lib/systemd/system/yum-autoupdate.service
%config /etc/logrotate.d/yum-autoupdate
%endif

%changelog
* Thu Dec 01 2022 Alex Iribarren <Alex.Iribarren@cern.ch> - 4.6-1-3
- change Requires: redhat-release to make the package more portable

* Tue May 11 2021 Ben Morrice <ben.morrice@cern.ch> - 4.6-1-2
- add BuildRequires: systemd

* Tue Apr 06 2021 Ben Morrice <ben.morrice@cern.ch> - 4.6-1-1
- use dnf-automatic on 8+ families
- remove support for SLC6 and below

* Mon Dec 03 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.5.1-2
- Fix logging on port 514 to http.
- Fix sed command removing the carriage returns

* Wed Nov 08 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.5.0
- remove yum-firstboot

* Wed Nov 08 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.4.7
- fixing systemd unit

* Tue Mar 01 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.4.5
- fixing nmap(-ncat) detection.

* Mon Feb 29 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.4.4
- fixed systemd file.

* Wed Jul 16 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.4-1
- adapt for el7
- systemd handling for el7

* Thu Feb 06 2014  Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.3.1-1
- label cron job as config
- add YUMUPDATEMETHOD for allowing use of 'yum distro-sync'
  instead of 'yum update'

* Wed Oct 10 2012 Thomas Oulevey <thomas.oulevey@cern.ch> - 4.3-5
- fixed empty email on slc6

* Wed Oct 05 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.3.4
- added preun scriplet in spec.

* Mon Jul 11 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.3
- added YUMUPDATESECONLY option on slc5/6 - to apply only
  security updates if enabled.

* Thu Feb 10 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 4.2.1
- handle plymouth on boot for SLC6

* Tue Nov  9 2010  KELEMEN Peter <Peter.Kelemen@cern.ch> - 4.2-3
- force autoupdate time window stretch

* Wed Oct  6 2010  KELEMEN Peter <Peter.Kelemen@cern.ch> - 4.2-2
- stretch autoupdate time window to one hour instead of 10 mins

* Mon Aug 23 2010 Jan van Eldik <Jan.van.Eldik@cern.ch> - 4.2-1
- fix count_changes() to properly report updates/upgrades on SLC4 and SLC5

* Thu Jan 28 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 4.1-2
- default dist tag is .slc5

* Wed Jan 27 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 4.1-1
- protect get_version() better against failures

* Tue Jan 26 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 4-0
- unified SLC4/SLC5 handling
- cleaned up dependencies
- got rid of obsolete apt->yum transition triggers

* Fri Jan 22 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3-7.slc5.cern
- Implemented partial change counting for check-update mode.

* Wed Jan 20 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3-6.slc5.cern
- Improved change number parsing.

* Thu Jan 14 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3-5.5.slc5.cern
- further tightening of output filtering

* Thu Jan 14 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3-5.2.slc5.cern
- fix backtracking grep(1) patterns for faster execution (Closes: CT652393)

* Fri Nov 20 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 3.5.1.slc5.cern
- enabled --skip-broken option
- remade logging
- cleaned out some mail output

* Wed Mar 18 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 3.1-4.slc5.cern
- addded another yum error detection.

* Fri Sep 05 2008  Jaroslaw Polok <jaroslaw.polok@cern.ch> - 3.1-3.slc5.cern
- added dep for yum-kernel-module

* Mon Aug  4 2008  Jan Iven <jan.iven@cern.ch> - 3.1-2.slc5.cern
- remove dep on installonlyn

* Wed Jun 18 2008 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 3.1
- initial release for SLC5

* Wed Oct 10 2007 KELEMEN Peter <Peter.Kelemen@cern.ch> - 2.9.slc4.cern
- spread autoupdate random sleep across 30 minutes on seconds granularity

* Mon Mar 5 2007 Jan Iven <jan.iven@cern.ch> - 2.8.slc4.cern
- detect some other YUM errors
- switch from "ping" to "nc" for server reachable test

* Tue Apr 25 2006  KELEMEN Peter <Peter.Kelemen> - 2-7.slc4.cern
- fix hidden dependency on expect

* Thu Jan 26 2006 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fixed problem with grep -P segfaults ...
- adjusted /etc/yum.conf to INCLUDE kernel-module-*

* Fri Jul 22 2005 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fixed bogus messages about updates (which were not there ...)

* Fri Feb 04 2005 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- adapt for both yum 2.0.X and 2.1.X

* Fri Jan 28 2005 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fixed few typos
* Fri Jan 14 2005 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- modeled after apt-autoupdate, initial release
* Fri Nov 05 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- added CERN stats logging 'a la SUE'
* Thu Jul 15 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- some cleanup, added on boot hookup
* Wed Jul 07 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fixed typo in /etc/init.d/apt-autoupdate
* Mon Jun 28 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fixed cleaning up of cron files
* Wed Jun 23 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- fixed upgrade-kernel command
* Tue Jun 08 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- repackaged for Scientific Linux
- site specific part is provided now by apt-sourceslist rpm
  (provided separately)
* Fri Apr 16 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- updated kernel-upgrade LUA script
* Thu Feb 26 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- added groupinstall & list-extras lua scripts
* Tue Feb 17 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- added kernel-upgrade.lua & cronjob
* Tue Feb 10 2004 Jaroslaw Polok <jaroslaw.polok@cern.ch>
- built new version.

